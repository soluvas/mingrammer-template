from diagrams import Cluster, Diagram
from diagrams.generic.storage import Storage
from diagrams.generic.os import Ubuntu
from diagrams.generic.network import Firewall, VPN
from diagrams.aws.compute import Lambda
from diagrams.aws.engagement import SimpleEmailServiceSes
from diagrams.aws.network import ElbApplicationLoadBalancer, APIGateway
from diagrams.alibabacloud.application import DirectMail, ApiGateway, OpenSearch
from diagrams.alibabacloud.database import ApsaradbPostgresql, ApsaradbRedis, RelationalDatabaseService
from diagrams.alibabacloud.compute import ContainerService
from diagrams.programming.language import Nodejs
from diagrams.programming.framework import React
from diagrams.saas.cdn import Cloudflare
from diagrams.onprem.monitoring import Grafana
from diagrams.alibabacloud.network import ServerLoadBalancer, VirtualPrivateCloud
from diagrams.alibabacloud.storage import ObjectStorageService
from diagrams.alibabacloud.security import ManagedSecurityService
from diagrams.custom import Custom

with Diagram("Template", show=False):
    cloudflare_cdn = Custom("Cloudflare CDN", "./icons/cloudflare.png")
    betteruptime = Custom("BetterUptime", "./icons/betteruptime2.png")
    google_analytics = Custom(
        "Google Analytics", "./icons/google-analytics.png")
    google_search_console = Custom(
        "Google Search Console", "./icons/google-search-console.png")
