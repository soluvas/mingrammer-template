# mingrammer template

## Requirements

In Gitpod:

- `.gitpod.yml` and `.gitpod.Dockerfile` is required due to graphviz dependency.
   You will need to merge it with your own Gitpod configuration files.

Locally:

- GraphViz (MacOS: `brew install graphviz`)
